# Installer Ionic : 
```
    npm i -g @ionic/cli@7
```

# Créer un projet Ionic : 
```
    ionic start nomProjet
```

# Lancer le projet Ionic :
```
    ionic serve
    ou
    ionic s
```