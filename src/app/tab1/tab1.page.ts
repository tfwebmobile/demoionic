import { Component } from '@angular/core';
import { ActionSheetController, IonicModule } from '@ionic/angular';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';
import { Product } from './product';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
  standalone: true,
  imports: [IonicModule, ExploreContainerComponent, CommonModule, FormsModule],
})
export class Tab1Page {
  constructor(private _actionSheet : ActionSheetController) { }

  productToAdd: string | undefined;

  productList: Product[] = [
    { name: 'Banane', isChecked: false },
    { name: 'Chocolat', isChecked: true }
  ]

  add(): void {
    // ? Est-ce que productToAdd est rempli et n'est pas vide ?
    if (this.productToAdd && this.productToAdd.trim() !== '') {

      // ? Est-ce qu'il n'est pas déjà dedans toute casse confondue ?
      // productToAdd?.maMethode() -> La méthode ne se fera que si productToAdd est non null et non undefined
      if (!this.productList.find(p => p.name.toLowerCase() === this.productToAdd?.trim().toLowerCase())) {

        //Ajout de productToAdd dans le tableau
        this.productList = [...this.productList, { name: this.productToAdd, isChecked: false }];
        //ou
        //this.productList.push({ name : this.productToAdd, isChecked : false})

      }      
    }
    //Refresh de productToAdd pour vider l'input
    this.productToAdd = undefined;
  }

  async openActionSheet(product : Product) {
    //On créé notre menu ActionSheet
    const myActionSheet = await this._actionSheet.create({
      header : product.name ,
      buttons : [
        {
          text : product.isChecked ? 'Décocher' : 'Cocher',
          icon : product.isChecked ? 'square-outline' : 'checkbox-outline',
          handler : () => { product.isChecked = !product.isChecked }
        },
        {
          text : 'Annuler',
          icon : 'close',
          role : 'cancel' //Pour IOS
        },
        {
          text : 'Supprimer',
          icon : 'trash',
          role : 'destructive', //Pour IOS
          handler : () => {
            this.productList = this.productList.filter(p => p !== product);
          }
        }
      ]
    })

    //On l'affiche à l'écran
    myActionSheet.present()
  }
}
